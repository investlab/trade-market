package org.bitbucket.tradedom.trade.market;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;

import static java.util.Comparator.comparing;
import static java.util.Objects.requireNonNull;
import static org.bitbucket.tradedom.trade.market.Price.ZERO_PRICE;
import static org.bitbucket.tradedom.trade.market.Volume.ZERO_VOLUME;

/**
 * Перечень предложений от продавцов.
 * По умолчанию без сортировки.
 */
public final class Asks implements Iterable<Ask> {
  private final List<Ask> asks;
  private final Sort sort;

  private Asks(List<Ask> asks, Sort sort) {
    this.asks = requireNonNull(asks);
    this.sort = requireNonNull(sort);
  }

  public Asks(Iterable<Ask> asks) {
    this(constructAskList(requireNonNull(asks)), Sort.NONE);
  }

  public Asks(Ask... asks) {
    this(Arrays.asList(asks), Sort.NONE);
  }

  public Asks(List<Ask> asks) {
    this(asks, Sort.NONE);
  }

  public Asks raisePrice(List<Amount> adds) {
    if (adds.isEmpty())
      return this;

    List<Ask> asks = new ArrayList<>(this.asks.size());
    Iterator<Amount> add = adds.iterator();
    for (Ask ask : this.asks)
      asks.add(add.hasNext() ? ask.raisePrice(add.next()) : ask);
    return new Asks(asks, sort);
  }

  /**
   * Разложить аски по уровням
   */
  public Asks tier(List<Volume> tiers) {
    List<Ask> asks = new ArrayList<>(tiers.size());
    for (Volume level : tiers) {
      Ask best = null;
      for (Ask ask : sortByVolumeAsc()) {
        if (ask.volume.greaterOrEqualThan(level)) {
          if (best == null || ask.betterThan(best))
            best = ask;
        }
      }
      asks.add(new Ask(best != null ? best.price : ZERO_PRICE, level));
    }
    return new Asks(asks, Sort.VOLUME_ASC);
  }

  /**
   * Вернуть наименьшую цену на покупку указанного объёма
   * Если предложений не хватает, возвращается наибольшая цена.
   * Если предложений нет, возвращается {@link Price#ZERO_PRICE}
   */
  public Price getVolumeLowestPrice(Volume volume) {
    if (volume.isZero())
      return ZERO_PRICE;

    Ask lastAsk = null;
    Volume sum = ZERO_VOLUME;
    for (Ask ask : sortByPriceAsc()) {
      lastAsk = ask;
      sum = sum.plus(ask.volume);
      if (sum.greaterOrEqualThan(volume))
        return ask.price;
    }
    return lastAsk != null ? lastAsk.price : ZERO_PRICE;
  }

  /**
   * Вернуть максимальный объём, который можно купить по указанной цене
   * или {@link Volume#ZERO_VOLUME}, если цена ниже предложений
   */
  public Volume getMaxVolumeToBuy(Price price) {
    if (price.isZero())
      return ZERO_VOLUME;

    Volume sum = ZERO_VOLUME;
    for (Ask ask : sortByPriceAsc()) {
      if (ask.price.greaterThan(price))
        break;
      sum = sum.plus(ask.volume);
    }
    return sum;
  }

  public Asks sortByVolumeAsc() {
    if (sort != Sort.VOLUME_ASC) {
      List<Ask> sorted = new ArrayList<>(asks);
      sorted.sort(((ask1, ask2) -> ask1.volume.compareTo(ask2.volume)));
      return new Asks(sorted, Sort.VOLUME_ASC);
    }
    return this;
  }

  public Asks sortByPriceAsc() {
    if (sort != Sort.PRICE_ASC) {
      List<Ask> sorted = new ArrayList<>(asks);
      sorted.sort(comparing((Ask ask) -> ask.price));
      return new Asks(sorted, Sort.PRICE_ASC);
    }
    return this;
  }

  public Asks sortByPriceDesc() {
    if (sort != Sort.PRICE_DESC) {
      List<Ask> sorted = new ArrayList<>(asks);
      sorted.sort(comparing((Ask ask) -> ask.price).reversed());
      return new Asks(sorted, Sort.PRICE_DESC);
    }
    return this;
  }

  public boolean isEmpty() {
    return asks.isEmpty();
  }

  public int size() {
    return asks.size();
  }

  @Override
  public Iterator<Ask> iterator() {
    return asks.iterator();
  }

  @Override
  public int hashCode() {
    return asks.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return o == this || (
      o != null
        && o.getClass() == Asks.class
        && ((Asks) o).asks.equals(this.asks)
    );
  }

  @Override
  public String toString() {
    StringJoiner joiner = new StringJoiner(",");
    for (Ask ask : asks)
      joiner.add("{" + ask.price + "," + ask.volume + "}");
    return "[" + joiner + "]";
  }

  private static List<Ask> constructAskList(Iterable<Ask> asks) {
    List<Ask> list = new ArrayList<>();
    for (Ask ask : asks)
      list.add(ask);
    return list;
  }
}
