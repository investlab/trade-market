package org.bitbucket.tradedom.trade.market;

import java.math.BigDecimal;

import static java.util.Objects.requireNonNull;

/**
 *
 */
public final class Volume implements Comparable<Volume> {
  public static final Volume ZERO_VOLUME = new Volume(Amount.ZERO);

  private final Amount amount;

  public Volume(Amount amount) {
    this.amount = requireNonNull(amount);
  }

  public Volume(BigDecimal amount) {
    this.amount = new Amount(amount);
  }

  public Volume(double amount) {
    this.amount = new Amount(amount);
  }

  public Volume(String amount) {
    this.amount = new Amount(amount);
  }

  public Volume plus(Volume add) {
    return clone(this.amount.plus(add.amount));
  }

  private Volume clone(Amount amount) {
    return amount == this.amount ? this : new Volume(amount);
  }

  public boolean isZero() {
    return amount.isZero();
  }

  public boolean greaterThan(Volume other) {
    return amount.greaterThan(other.amount);
  }

  public boolean greaterOrEqualThan(Volume other) {
    return amount.greaterOrEqualThan(other.amount);
  }

  public boolean lessThan(Volume other) {
    return amount.lessThan(other.amount);
  }

  @Override
  public int hashCode() {
    return amount.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return o == this || (
      o != null
        && o.getClass() == Volume.class
        && ((Volume) o).amount.equals(this.amount)
    );
  }

  @Override
  public int compareTo(Volume other) {
    return amount.compareTo(other.amount);
  }

  @Override
  public String toString() {
    return amount.toString();
  }
}
