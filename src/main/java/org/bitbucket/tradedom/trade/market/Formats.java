package org.bitbucket.tradedom.trade.market;

import java.time.format.DateTimeFormatter;

/**
 *
 */
interface Formats {
  DateTimeFormatter TIME_FORMAT =
    DateTimeFormatter.ofPattern("HH:mm:ss");
}
