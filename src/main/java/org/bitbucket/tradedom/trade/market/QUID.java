package org.bitbucket.tradedom.trade.market;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Идентификатор котировки, уникальный в рамках процесса.
 * Используется для логирования котировки
 */
public final class QUID {
  private static final AtomicLong counter = new AtomicLong(1);

  private final String value;

  QUID(Instrument instrument) {
    this.value = instrument + "_" + counter.incrementAndGet();
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(value);
  }

  @Override
  public boolean equals(Object obj) {
    return this == obj || (
      obj != null &&
        obj.getClass() == QUID.class
        && ((QUID) obj).value.equals(this.value)
    );
  }

  @Override
  public String toString() {
    return value;
  }
}
