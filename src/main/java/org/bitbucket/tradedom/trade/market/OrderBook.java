package org.bitbucket.tradedom.trade.market;

import java.time.LocalDateTime;
import java.util.StringJoiner;

import static java.util.Objects.requireNonNull;
import static org.bitbucket.tradedom.trade.market.Formats.TIME_FORMAT;

/**
 * Стакан заявок
 */
public final class OrderBook {
  public final Instrument instrument;
  public final LocalDateTime time;
  public final QUID quid;
  public final Asks asks;
  public final Bids bids;

  public OrderBook(Instrument instrument, LocalDateTime time, Asks asks, Bids bids) {
    this.quid = new QUID(instrument);
    this.time = requireNonNull(time);
    this.asks = requireNonNull(asks);
    this.bids = requireNonNull(bids);
    this.instrument = requireNonNull(instrument);
  }

  /**
   * @return true, если асков ИЛИ бидов нет
   */
  public boolean isEmpty() {
    return asks.isEmpty() || bids.isEmpty();
  }

  /**
   * Вернуть лучшую (наименьшую) цену на покупку указанного объёма.
   * Если предложений не хватает, возвращается худшая (наибольшая) цена.
   * Если предложений нет, возвращается {@link Price#ZERO_PRICE}
   */
  public Price getBestAskPrice(Volume volume) {
    return asks.getVolumeLowestPrice(volume);
  }

  public Price getBestAskPrice(int quantity) {
    return getBestAskPrice(new Volume(quantity));
  }

  /**
   * Вернуть лучшую (наибольшую) цену на продажу указанного объёма.
   * Если предложений не хватает, возвращается худшая (наименьшая) цена.
   * Если предложений нет, возвращается {@link Price#ZERO_PRICE}
   */
  public Price getBestBidPrice(Volume volume) {
    return bids.getVolumeHighestPrice(volume);
  }

  public Price getBestBidPrice(int quantity) {
    return bids.getVolumeHighestPrice(new Volume(quantity));
  }

  /**
   * Вернуть максимальный объём, который можно купить по указанной цене
   * или {@link Volume#ZERO_VOLUME}, если цена ниже предложений
   */
  public Volume getMaxVolumeToBuy(Price price) {
    return asks.getMaxVolumeToBuy(price);
  }

  /**
   * Вернуть максимальный объём, который можно продать по указанной цене
   * или {@link Volume#ZERO_VOLUME}, если цена ниже предложений
   */
  public Volume getMaxVolumeToSell(Price price) {
    return bids.getMaxVolumeToSell(price);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ")
      .add(instrument.toString())
      .add(time.format(TIME_FORMAT))
      .add("asks=" + asks)
      .add("bids=" + bids)
      .toString();
  }
}
