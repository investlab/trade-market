package org.bitbucket.tradedom.trade.market;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Ворота.
 * Используется в усреднении котировок методом расширения спреда
 */
public final class Gate {
  public final Price bid;
  public final Price ask;

  Gate(Price bid, Price ask) {
    this.bid = requireNonNull(bid);
    this.ask = requireNonNull(ask);
  }

  @Override
  public int hashCode() {
    return Objects.hash(bid);
  }

  @Override
  public boolean equals(Object obj) {
    return this == obj || (
      obj != null
        && obj.getClass() == Gate.class
        && ((Gate) obj).ask.equals(this.ask)
        && ((Gate) obj).bid.equals(this.bid)
    );
  }

  @Override
  public String toString() {
    return "[" + bid + ", " + ask + "]";
  }
}
