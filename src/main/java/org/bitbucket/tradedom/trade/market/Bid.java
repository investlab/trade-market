package org.bitbucket.tradedom.trade.market;

import static java.util.Objects.requireNonNull;

/**
 * Бид: предложение от покупателя
 * Бид меньше чем Аск
 */
public final class Bid {
  public final Price price;
  public final Volume volume;

  public Bid(Price price, Volume volume) {
    this.price = requireNonNull(price);
    this.volume = requireNonNull(volume);
  }

  public Bid(Price price, int volume) {
    this(price, new Volume(volume));
  }

  public Bid(double price, double volume) {
    this(new Price(price), new Volume(volume));
  }

  public Bid reducePrice(Amount sub) {
    return sub.isZero() ? this
      : new Bid(price.minus(sub), volume);
  }

  public boolean betterThan(Bid other) {
    return price.greaterThan(other.price);
  }

  @Override
  public int hashCode() {
    return volume.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return o == this || (
      o != null
        && o.getClass() == Bid.class
        && ((Bid) o).price.equals(this.price)
        && ((Bid) o).volume.equals(this.volume)
    );
  }

  @Override
  public String toString() {
    return volume + "@" + price;
  }
}
