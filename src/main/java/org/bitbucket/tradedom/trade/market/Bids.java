package org.bitbucket.tradedom.trade.market;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;

import static java.util.Comparator.comparing;
import static java.util.Objects.requireNonNull;
import static org.bitbucket.tradedom.trade.market.Price.ZERO_PRICE;
import static org.bitbucket.tradedom.trade.market.Volume.ZERO_VOLUME;

/**
 * Перечень предложений от покупателей.
 * По умолчанию без сортировки.
 */
public final class Bids implements Iterable<Bid> {
  private final List<Bid> bids;
  private final Sort sort;

  private Bids(List<Bid> bids, Sort sort) {
    this.bids = requireNonNull(bids);
    this.sort = requireNonNull(sort);
  }

  public Bids(Iterable<Bid> bids) {
    this(constructBidList(requireNonNull(bids)), Sort.NONE);
  }

  public Bids(Bid... bids) {
    this(Arrays.asList(bids), Sort.NONE);
  }

  public Bids(List<Bid> bids) {
    this(bids, Sort.NONE);
  }

  public Bids reducePrice(List<Amount> subs) {
    if (subs.isEmpty())
      return this;

    List<Bid> bids = new ArrayList<>(this.bids.size());
    Iterator<Amount> sub = subs.iterator();
    for (Bid bid : this.bids)
      bids.add(sub.hasNext() ? bid.reducePrice(sub.next()) : bid);
    return new Bids(bids, sort);
  }

  /**
   * Разложить биды по уровням
   */
  public Bids tier(List<Volume> tiers) {
    List<Bid> bids = new ArrayList<>();
    for (Volume level : tiers) {
      Bid best = null;
      for (Bid bid : sortByVolumeAsc()) {
        if (bid.volume.greaterOrEqualThan(level)) {
          if (best == null || bid.betterThan(best))
            best = bid;
        }
      }
      bids.add(new Bid(best != null ? best.price : ZERO_PRICE, level));
    }
    return new Bids(bids, Sort.VOLUME_ASC);
  }

  /**
   * Вернуть наибольшую цену на продажу указанного объёма.
   * Если предложений не хватает, возвращается наименьшая цена.
   * Если предложений нет, возвращается {@link Price#ZERO_PRICE}
   */
  public Price getVolumeHighestPrice(Volume volume) {
    if (volume.isZero())
      return ZERO_PRICE;

    Bid lastBid = null;
    Volume sum = ZERO_VOLUME;
    for (Bid bid : sortByPriceDesc()) {
      lastBid = bid;
      sum = sum.plus(bid.volume);
      if (sum.greaterOrEqualThan(volume))
        return bid.price;
    }
    return lastBid != null ? lastBid.price : ZERO_PRICE;
  }

  /**
   * Вернуть максимальный объём, который можно продать по указанной цене
   * или {@link Volume#ZERO_VOLUME}, если цена выше предложений
   */
  public Volume getMaxVolumeToSell(Price price) {
    if (price.isZero())
      return ZERO_VOLUME;

    Volume sum = ZERO_VOLUME;
    for (Bid bid : sortByPriceDesc()) {
      if (bid.price.lessThan(price))
        break;
      sum = sum.plus(bid.volume);
    }
    return sum;
  }

  public Bids sortByPriceDesc() {
    if (sort != Sort.PRICE_DESC) {
      List<Bid> sorted = new ArrayList<>(bids);
      sorted.sort(comparing((Bid bid) -> bid.price).reversed());
      return new Bids(sorted, Sort.PRICE_DESC);
    }
    return this;
  }

  public Bids sortByVolumeAsc() {
    if (sort != Sort.VOLUME_ASC) {
      List<Bid> sorted = new ArrayList<>(bids);
      sorted.sort(((bid1, bid2) -> bid1.volume.compareTo(bid2.volume)));
      return new Bids(sorted, Sort.VOLUME_ASC);
    }
    return this;
  }

  public boolean isEmpty() {
    return bids.isEmpty();
  }

  public int size() {
    return bids.size();
  }

  @Override
  public Iterator<Bid> iterator() {
    return bids.iterator();
  }

  @Override
  public int hashCode() {
    return bids.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return o == this || (
      o != null
        && o.getClass() == Bids.class
        && ((Bids) o).bids.equals(this.bids)
    );
  }

  @Override
  public String toString() {
    StringJoiner joiner = new StringJoiner(",");
    for (Bid bid : bids)
      joiner.add("{" + bid.price + "," + bid.volume + "}");
    return "[" + joiner + "]";
  }

  private static List<Bid> constructBidList(Iterable<Bid> bids) {
    List<Bid> list = new ArrayList<>();
    for (Bid bid : bids)
      list.add(bid);
    return list;
  }
}
