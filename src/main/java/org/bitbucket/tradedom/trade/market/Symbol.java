package org.bitbucket.tradedom.trade.market;

/**
 * Символ
 * Например, USD, BTC
 */
public final class Symbol {
  private final String name;

  public Symbol(String name) {
    if (name == null || name.trim().isEmpty())
      throw new IllegalArgumentException("Symbol cannot be blank");
    this.name = name;
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return o == this || (
      o != null
        && o.getClass() == Symbol.class
        && ((Symbol) o).name.equals(this.name)
    );
  }

  @Override
  public String toString() {
    return name;
  }
}
