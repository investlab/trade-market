package org.bitbucket.tradedom.trade.market;

/**
 * Направление сделки
 */
public enum Side {
  BUY,
  SELL
}
