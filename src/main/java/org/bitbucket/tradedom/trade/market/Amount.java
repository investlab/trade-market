package org.bitbucket.tradedom.trade.market;

import java.math.BigDecimal;
import java.math.MathContext;

import static java.math.RoundingMode.HALF_UP;
import static java.util.Objects.requireNonNull;

/**
 *
 */
public final class Amount extends Number implements Comparable<Amount> {
  public static final MathContext MATH_CONTEXT = new MathContext(10, HALF_UP);
  public static final Amount ZERO = new Amount(BigDecimal.ZERO);
  public static final Amount MAX = new Amount(new BigDecimal(Long.MAX_VALUE));
  public static final Amount ZERO_AMOUNT = ZERO;
  public static final Amount MAX_AMOUNT = MAX;

  private final BigDecimal value;

  public Amount(BigDecimal value) {
    this.value = requireNonNull(value);
  }

  public Amount(double value) {
    this.value = BigDecimal.valueOf(value);
  }

  public Amount(String value) {
    if (value == null || value.trim().isEmpty())
      throw new IllegalArgumentException("Amount cannot be blank");
    this.value = new BigDecimal(value);
  }

  public Amount plus(Amount add) {
    return isZero() ? add
      : add.isZero() ? this
      : new Amount(value.add(add.value, MATH_CONTEXT));
  }

  public Amount plus(double add) {
    return new Amount(value.add(BigDecimal.valueOf(add), MATH_CONTEXT));
  }

  public Amount minus(Amount sub) {
    return sub.isZero() ? this
      : new Amount(value.subtract(sub.value, MATH_CONTEXT));
  }

  public Amount minus(double sub) {
    return new Amount(value.subtract(BigDecimal.valueOf(sub), MATH_CONTEXT));
  }

  public Amount mul(Amount mul) {
    return isZero() ? ZERO
      : mul.isZero() ? ZERO
      : new Amount(value.multiply(mul.value, MATH_CONTEXT));
  }

  public Amount mul(int mul) {
    if (isZero())
      return ZERO;
    switch (mul) {
      case 0:
        return ZERO;
      case 1:
        return this;
      default:
        return new Amount(value.multiply(BigDecimal.valueOf(mul), MATH_CONTEXT));
    }
  }

  public Amount div(int div) throws ArithmeticException {
    if (isZero())
      return ZERO;
    switch (div) {
      case 0:
        throw new ArithmeticException("Cannot divide by zero");
      case 1:
        return this;
      default:
        return new Amount(value.divide(BigDecimal.valueOf(div), MATH_CONTEXT));
    }
  }

  public Amount round(int precision) {
    if (precision >= MATH_CONTEXT.getPrecision())
      return this;

    return new Amount(
      value.setScale(
        precision,
        MATH_CONTEXT.getRoundingMode()
      )
    );
  }

  public Amount negative() {
    return isZero() ? ZERO : new Amount(value.negate());
  }

  public boolean isZero() {
    return value.signum() == 0;
  }

  public boolean isNonZero() {
    return value.signum() != 0;
  }

  public boolean isPositive() {
    return value.signum() > 0;
  }

  public boolean isNonPositive() {
    return value.signum() <= 0;
  }

  public boolean isNegative() {
    return value.signum() < 0;
  }

  public boolean isNonNegative() {
    return value.signum() >= 0;
  }

  public boolean greaterThan(Amount other) {
    return compareTo(other) > 0;
  }

  public boolean greaterOrEqualThan(Amount other) {
    return compareTo(other) >= 0;
  }

  public boolean lessThan(Amount other) {
    return compareTo(other) < 0;
  }

  public boolean between(Amount min, Amount max) {
    return this.compareTo(min) >= 0 && this.compareTo(max) <= 0;
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return o == this || (
      o != null
        && o.getClass() == Amount.class
        && compareTo((Amount) o) == 0
    );
  }

  @Override
  public int compareTo(Amount other) {
    return value.compareTo(other.value);
  }

  @Override
  public String toString() {
    return value.toPlainString();
  }

  @Override
  public int intValue() {
    return value.intValue();
  }

  @Override
  public long longValue() {
    return value.longValue();
  }

  @Override
  public float floatValue() {
    return value.floatValue();
  }

  @Override
  public double doubleValue() {
    return value.doubleValue();
  }
}
