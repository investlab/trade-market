package org.bitbucket.tradedom.trade.market;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

import static org.bitbucket.tradedom.trade.market.Formats.TIME_FORMAT;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 *
 */
public final class Quote {
  public final Bid bid;
  public final Ask ask;
  public final QUID quid;
  public final LocalDateTime time;
  public final Instrument instrument;

  public Quote(OrderBook book, Volume minVolume) {
    if (book.isEmpty())
      throw new IllegalStateException("Стакан пуст!");

    Price bidPrice = book.getBestBidPrice(minVolume);
    Price askPrice = book.getBestAskPrice(minVolume);
    Volume bidVolume = book.getMaxVolumeToSell(bidPrice);
    Volume askVolume = book.getMaxVolumeToBuy(askPrice);

    this.instrument = book.instrument;
    this.bid = new Bid(bidPrice, bidVolume);
    this.ask = new Ask(askPrice, askVolume);
    this.time = book.time;
    this.quid = book.quid;
  }

  Quote(QUID quid, Instrument instrument, Bid bid, Ask ask, LocalDateTime time) {
    this.time = requireNonNull(time);
    this.bid = requireNonNull(bid);
    this.ask = requireNonNull(ask);
    this.quid = requireNonNull(quid);
    this.instrument = requireNonNull(instrument);
  }

  public Gate toGate() {
    return new Gate(bid.price, ask.price);
  }

  /**
   * @return true, если котировка попадает в ворота
   */
  public boolean hit(Gate gate) {
    return this.bid.price.between(gate.bid, gate.ask)
      && this.ask.price.between(gate.bid, gate.ask);
  }

  public Quote reserve(Amount reserve) {
    return new Quote(
      quid,
      instrument,
      bid.reducePrice(reserve),
      ask.raisePrice(reserve),
      time);
  }

  public Quote withQuid(QUID quid) {
    return new Quote(quid, instrument, bid, ask, time);
  }

  @Override
  public int hashCode() {
    return Objects.hash(instrument);
  }

  @Override
  public boolean equals(Object obj) {
    return this == obj || (
      obj != null
        && obj.getClass() == Quote.class
        && ((Quote) obj).instrument.equals(this.instrument)
        && ((Quote) obj).ask.equals(this.ask)
        && ((Quote) obj).bid.equals(this.bid)
    );
  }

  @Override
  public String toString() {
    return new StringJoiner(", ")
      .add(instrument.toString())
      .add(time.format(TIME_FORMAT))
      .add(bid.toString())
      .add(ask.toString())
      .toString();
  }
}
