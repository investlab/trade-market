package org.bitbucket.tradedom.trade.market;

import java.math.BigDecimal;

import static java.util.Objects.requireNonNull;

/**
 *
 */
public final class Price implements Comparable<Price> {
  public static final Price ZERO_PRICE = new Price(Amount.ZERO);

  private final Amount amount;

  public Price(Amount amount) {
    this.amount = requireNonNull(amount);
  }

  public Price(BigDecimal amount) {
    this.amount = new Amount(amount);
  }

  public Price(double value) {
    this.amount = new Amount(value);
  }

  public Price(String amount) {
    this.amount = new Amount(amount);
  }

  public Price plus(Price add) {
    return clone(amount.plus(add.amount));
  }

  public Price plus(double add) {
    return clone(amount.plus(add));
  }

  public Price plus(Amount add) {
    return clone(amount.plus(add));
  }

  public Price minus(Price sub) {
    return clone(amount.minus(sub.amount));
  }

  public Price minus(double sub) {
    return clone(amount.minus(sub));
  }

  public Price minus(Amount sub) {
    return clone(amount.minus(sub));
  }

  public Price mul(Amount mul) {
    return clone(amount.mul(mul));
  }

  public Price mul(double mul) {
    return clone(amount.mul(new Amount(mul)));
  }

  public Price mul(int mul) {
    return clone(amount.mul(mul));
  }

  public Price div(int div) throws ArithmeticException {
    return clone(amount.div(div));
  }

  public Price round(int precision) {
    return clone(amount.round(precision));
  }

  private Price clone(Amount amount) {
    return amount == this.amount ? this : new Price(amount);
  }

  public boolean isZero() {
    return amount.isZero();
  }

  public boolean greaterThan(Price other) {
    return amount.greaterThan(other.amount);
  }

  public boolean lessThan(Price other) {
    return amount.lessThan(other.amount);
  }

  public boolean between(Price min, Price max) {
    return amount.between(min.amount, max.amount);
  }

  @Override
  public int hashCode() {
    return amount.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return o == this || (
      o != null
        && o.getClass() == Price.class
        && ((Price) o).amount.equals(this.amount)
    );
  }

  @Override
  public int compareTo(Price other) {
    return amount.compareTo(other.amount);
  }

  @Override
  public String toString() {
    return amount.toString();
  }
}
