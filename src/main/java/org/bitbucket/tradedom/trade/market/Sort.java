package org.bitbucket.tradedom.trade.market;

/**
 *
 */
enum Sort {
  NONE,
  VOLUME_ASC,
  PRICE_ASC,
  PRICE_DESC
}
