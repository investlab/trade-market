package org.bitbucket.tradedom.trade.market;

import static java.util.Objects.requireNonNull;

/**
 * Аск: предложение от продавца.
 * Аск больше чем Бид
 */
public final class Ask {
  public final Price price;
  public final Volume volume;

  public Ask(Price price, Volume volume) {
    this.price = requireNonNull(price);
    this.volume = requireNonNull(volume);
  }

  public Ask(Price price, int volume) {
    this(price, new Volume(volume));
  }

  public Ask(double price, double volume) {
    this(new Price(price), new Volume(volume));
  }

  public Ask raisePrice(Amount add) {
    return add.isZero() ? this
      : new Ask(price.plus(add), volume);
  }

  /**
   * @return true, если цена у аска this лучше (меньше) цены аска other
   */
  public boolean betterThan(Ask other) {
    return price.lessThan(other.price);
  }

  @Override
  public int hashCode() {
    return volume.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return o == this || (
      o != null
        && o.getClass() == Ask.class
        && ((Ask) o).price.equals(this.price)
        && ((Ask) o).volume.equals(this.volume)
    );
  }

  @Override
  public String toString() {
    return volume + "@" + price;
  }
}
