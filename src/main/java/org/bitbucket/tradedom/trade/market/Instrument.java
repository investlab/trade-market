package org.bitbucket.tradedom.trade.market;

import java.util.Objects;

import static org.bitbucket.tradedom.trade.market.Amount.ZERO_AMOUNT;
import static org.bitbucket.tradedom.trade.market.Volume.ZERO_VOLUME;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 *
 */
public final class Instrument {
  public final Symbol name;

  /**
   * Алиас инструмента на сервере
   */
  public final Symbol alias;

  /**
   * Минимальный объём для вычисления бида и аска котировки
   */
  public final Volume minVolume;

  /**
   * Запас для раздвижения спреда, когда котировка не попадает в "ворота"
   */
  public final Amount reserve;

  public Instrument(Symbol name, Symbol alias, Volume minVolume, Amount reserve) {
    this.name = requireNonNull(name);
    this.alias = requireNonNull(alias);
    this.minVolume = requireNonNull(minVolume);
    this.reserve = requireNonNull(reserve);
  }

  public Instrument(String name, String alias, Volume minVolume, Amount reserve) {
    this(new Symbol(name), new Symbol(alias), minVolume, reserve);
  }

  Instrument(String name) {
    this(name, name, ZERO_VOLUME, ZERO_AMOUNT);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(name);
  }

  @Override
  public boolean equals(Object obj) {
    return obj == this || (
      obj != null
        && obj.getClass() == Instrument.class
        && name.equals(((Instrument) obj).name)
        && alias.equals(((Instrument) obj).alias)
        && minVolume.equals(((Instrument) obj).minVolume)
        && reserve.equals(((Instrument) obj).reserve)
    );
  }

  @Override
  public String toString() {
    return alias.toString();
  }
}