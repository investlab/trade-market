package org.bitbucket.tradedom.trade.market;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 */
public class QuoteTest {
  private static final Instrument USD_RUB = new Instrument("USD/RUB");
  private final LocalDateTime time = LocalDateTime.now();

  @Test
  public void testQuoteFromBook() {
    OrderBook book = new OrderBook(
      USD_RUB,
      LocalDateTime.now(),
      new Asks(
        new Ask(73.7200, 300)
        , new Ask(73.7175, 199)
        , new Ask(73.7150, 92)
        , new Ask(73.7125, 145)
        , new Ask(73.7100, 100)
        , new Ask(73.7075, 100)
        , new Ask(73.7050, 102)
        , new Ask(73.7025, 150)
        , new Ask(73.7000, 150)
        , new Ask(73.6975, 199)
      ),
      new Bids(
        new Bid(73.6875, 411)
        , new Bid(73.6850, 1_612)
        , new Bid(73.6825, 200)
        , new Bid(73.6800, 210)
        , new Bid(73.6775, 304)
        , new Bid(73.6750, 186)
        , new Bid(73.6725, 53)
      )
    );

    Quote quoteFor10 = new Quote(book, new Volume(10));
    assertEquals(new Ask(73.6975, 199), quoteFor10.ask);
    assertEquals(new Bid(73.6875, 411), quoteFor10.bid);

    Quote quoteFor500 = new Quote(book, new Volume(500));
    assertEquals(new Ask(73.7050, 102 + 150 + 150 + 199), quoteFor500.ask);
    assertEquals(new Bid(73.6850, 411 + 1_612), quoteFor500.bid);

    Quote quoteFor1000 = new Quote(book, new Volume(1000));
    assertEquals(new Ask(73.7150, 199 + 150 + 150 + 102 + 100 + 100 + 145 + 92), quoteFor1000.ask);
    assertEquals(new Bid(73.6850, 411 + 1_612), quoteFor1000.bid);

    Quote quoteFor2500 = new Quote(book, new Volume(2500));
    assertEquals(new Ask(73.7200, 1537), quoteFor2500.ask);
    assertEquals(new Bid(73.6775, 411 + 1_612 + 200 + 210 + 304), quoteFor2500.bid);
  }

  @Test
  public void testHit() {
    assertTrue(quote(5, 15).hit(gate(0, 20)));
    assertTrue(quote(5, 15).hit(gate(5, 15)));
    assertFalse(quote(5, 15).hit(gate(10, 20)));
    assertFalse(quote(5, 15).hit(gate(11, 11)));
  }

  @Test
  public void testReserve() {
    assertEquals(quote(45, 65), quote(50, 60).reserve(new Amount(5)));
    assertEquals(quote(50, 60), quote(50, 60).reserve(new Amount(0)));
    assertEquals(quote(55, 55), quote(50, 60).reserve(new Amount(-5)));
  }

  private Quote quote(double bidPrice, double askPrice) {
    return new Quote(
      new QUID(USD_RUB),
      USD_RUB,
      new Bid(bidPrice, 0),
      new Ask(askPrice, 0),
      time
    );
  }

  private Gate gate(double bidPrice, double askPrice) {
    return new Gate(
      new Price(bidPrice),
      new Price(askPrice)
    );
  }
}
