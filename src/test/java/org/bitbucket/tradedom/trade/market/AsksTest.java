package org.bitbucket.tradedom.trade.market;

import org.junit.Test;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 */
public class AsksTest {
  private final Asks givenAsks = new Asks(
    new Ask(1, 100),
    new Ask(1, 100),
    new Ask(1, 100),
    new Ask(2, 200),
    new Ask(3, 300),
    new Ask(3, 300),
    new Ask(3, 300),
    new Ask(4, 300),
    new Ask(5, 500),
    new Ask(6, 300)
  );

  @Test
  public void testPrintAsks() {
    System.out.println(givenAsks);
  }

  @Test
  public void testGetVolumeLowestPrice() {
    Price actual = givenAsks.getVolumeLowestPrice(new Volume(1000));
    Price expected = new Price(3);
    assertEquals(actual, expected);
  }

  @Test
  public void testGetMaxVolumeToBuy() {
    Volume actual = givenAsks.getMaxVolumeToBuy(new Price(2));
    Volume expected = new Volume(500);
    assertEquals(actual, expected);
  }

  @Test
  public void testSortByVolume() {
    Asks given = new Asks(
      new Ask(1, 900),
      new Ask(1, 100),
      new Ask(1, 100),
      new Ask(2, 200),
      new Ask(3, 300),
      new Ask(3, 300),
      new Ask(3, 300),
      new Ask(4, 300),
      new Ask(5, 500),
      new Ask(6, 300)
    );
    Asks sorted = new Asks(
      new Ask(1, 100),
      new Ask(1, 100),
      new Ask(2, 200),
      new Ask(3, 300),
      new Ask(3, 300),
      new Ask(3, 300),
      new Ask(4, 300),
      new Ask(6, 300),
      new Ask(5, 500),
      new Ask(1, 900)
    );
    assertEquals(given.sortByVolumeAsc(), sorted);
  }

  @Test
  public void testSortByPrice() {
    Asks given = new Asks(
      new Ask(3, 300),
      new Ask(1, 900),
      new Ask(1, 100),
      new Ask(4, 300),
      new Ask(1, 100),
      new Ask(2, 200),
      new Ask(3, 300),
      new Ask(3, 300),
      new Ask(5, 500),
      new Ask(6, 300)
    );
    Asks sorted = new Asks(
      new Ask(1, 900),
      new Ask(1, 100),
      new Ask(1, 100),
      new Ask(2, 200),
      new Ask(3, 300),
      new Ask(3, 300),
      new Ask(3, 300),
      new Ask(4, 300),
      new Ask(5, 500),
      new Ask(6, 300)
    );
    assertEquals(given.sortByPriceAsc(), sorted);
  }

  @Test
  public void testIsEmpty() {
    assertTrue(new Asks(emptyList()).isEmpty());
    assertFalse(givenAsks.isEmpty());
  }

  @Test
  public void testSize() {
    assertEquals(0, new Asks(emptyList()).size());
    assertEquals(10, givenAsks.size());
  }
}
