package org.bitbucket.tradedom.trade.market;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 */
public class AskTest {
  @Test
  public void testRaisePrice() {
    assertEquals(new Ask(100, 1000).raisePrice(new Amount(21)), new Ask(121, 1000));
    assertEquals(new Ask(100, 1000).raisePrice(Amount.ZERO), new Ask(100, 1000));
  }

  @Test
  public void testBetterThen() {
    assertTrue(new Ask(100, 1000).betterThan(new Ask(200, 1000)));
    assertFalse(new Ask(99, 1000).betterThan(new Ask(98, 1000)));
  }
}
