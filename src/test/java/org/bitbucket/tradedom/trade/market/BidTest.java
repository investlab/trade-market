package org.bitbucket.tradedom.trade.market;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 */
public class BidTest {
  @Test
  public void testReducePrice() {
    assertEquals(new Bid(100, 1000).reducePrice(new Amount(21)), new Bid(79, 1000));
    assertEquals(new Bid(100, 1000).reducePrice(Amount.ZERO), new Bid(100, 1000));
  }

  @Test
  public void testBetterThen() {
    assertTrue(new Bid(100, 1000).betterThan(new Bid(97, 1000)));
    assertFalse(new Bid(99, 1000).betterThan(new Bid(99.1, 1000)));
  }
}
