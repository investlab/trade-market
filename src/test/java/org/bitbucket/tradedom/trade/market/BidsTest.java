package org.bitbucket.tradedom.trade.market;

import org.junit.Test;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 *
 */
public class BidsTest {
  private final Bids givenBids = new Bids(
    new Bid(1, 100),
    new Bid(1, 100),
    new Bid(1, 100),
    new Bid(2, 200),
    new Bid(3, 300),
    new Bid(3, 300),
    new Bid(3, 300),
    new Bid(4, 300),
    new Bid(5, 500),
    new Bid(6, 300)
  );

  @Test
  public void testPrintOrders() {
    System.out.println(givenBids);
  }

  @Test
  public void testGetVolumeHighestPrice() {
    Price actual = givenBids.getVolumeHighestPrice(new Volume(1000));
    Price expected = new Price(4);
    assertEquals(actual, expected);
  }

  @Test
  public void testGetMaxVolumeToSell() {
    Volume actual = givenBids.getMaxVolumeToSell(new Price(2));
    Volume expected = new Volume(2200);
    assertEquals(actual, expected);
  }

  @Test
  public void testSortByVolume() {
    Bids given = new Bids(
      new Bid(1, 900),
      new Bid(1, 100),
      new Bid(1, 100),
      new Bid(2, 200),
      new Bid(3, 300),
      new Bid(3, 300),
      new Bid(3, 300),
      new Bid(4, 300),
      new Bid(5, 500),
      new Bid(6, 300)
    );
    Bids sorted = new Bids(
      new Bid(1, 100),
      new Bid(1, 100),
      new Bid(2, 200),
      new Bid(3, 300),
      new Bid(3, 300),
      new Bid(3, 300),
      new Bid(4, 300),
      new Bid(6, 300),
      new Bid(5, 500),
      new Bid(1, 900)
    );
    assertEquals(given.sortByVolumeAsc(), sorted);
  }

  @Test
  public void testSortByPrice() {
    Bids given = new Bids(
      new Bid(3, 300),
      new Bid(1, 900),
      new Bid(7, 100),
      new Bid(4, 300),
      new Bid(1, 900),
      new Bid(2, 200),
      new Bid(3, 300),
      new Bid(3, 300),
      new Bid(5, 500),
      new Bid(6, 300)
    );
    Bids sorted = new Bids(
      new Bid(7, 100),
      new Bid(6, 300),
      new Bid(5, 500),
      new Bid(4, 300),
      new Bid(3, 300),
      new Bid(3, 300),
      new Bid(3, 300),
      new Bid(2, 200),
      new Bid(1, 900),
      new Bid(1, 900)
    );
    assertEquals(given.sortByPriceDesc(), sorted);
  }

  @Test
  public void testIsEmpty() {
    assertTrue(new Bids(emptyList()).isEmpty());
    assertFalse(givenBids.isEmpty());
  }

  @Test
  public void testSize() {
    assertEquals(0, new Bids(emptyList()).size());
    assertEquals(10, givenBids.size());
  }
}
