package org.bitbucket.tradedom.trade.market;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 */
public final class PriceTest {
  /**
   * Тестирование конвертации в строку иокто-цены (10^-24)
   */
  @Test
  public void testYoctoPrice() {
    String yocto = "0.000000000000000000000001";
    Price price = new Price(yocto);
    assertEquals(yocto, price.toString());
  }

  @Test
  public void testGigaYoctoPrice() {
    String gigaYocto = "980000000000.000000000000000000000001";
    Price price = new Price(gigaYocto);
    assertEquals(gigaYocto, price.toString());
  }

  @Test
  public void testEngineeringPrice() {
    assertEquals("12260", new Price("1.226E+4").toString());
    assertEquals("12150", new Price("1.215E+4").toString());
    assertEquals("9600", new Price("9.6E+3").toString());
    assertEquals("60", new Price("6E+1").toString());
  }

  @Test
  public void testEquals() {
    Assert.assertEquals(Price.ZERO_PRICE, Price.ZERO_PRICE);
    assertEquals(new Price("12.1"), new Price(12.1));
    assertNotEquals(new Price("12.1"), null);
    assertNotEquals(null, new Price("12.1"));
    assertNotEquals(new Price(1.111111111), new Price("1.1111111"));
  }

  @Test
  public void testPlus() {
    Assert.assertEquals(Price.ZERO_PRICE.plus(new Price("12.1")), new Price(12.1));
    assertEquals(new Price("2").plus(new Price(2)), new Price(4));
    assertNotEquals(new Price("1").plus(new Price("2")), new Price(4));
  }

  @Test
  public void testMinus() {
    Assert.assertEquals(Price.ZERO_PRICE, Price.ZERO_PRICE.minus(Price.ZERO_PRICE));
    assertEquals(new Price("1.1"), new Price(1.1).minus(Price.ZERO_PRICE));
    assertEquals(new Price(4), new Price("6").minus(new Price(2)));
    assertNotEquals(new Price(4), new Price("1").minus(new Price("2")));
  }

  @Test
  public void testMul() {
    Assert.assertEquals(Price.ZERO_PRICE, Price.ZERO_PRICE.mul(0));
    Assert.assertEquals(Price.ZERO_PRICE, Price.ZERO_PRICE.mul(1));
    assertEquals(new Price(4), new Price("2").mul(2));
    assertNotEquals(new Price(3), new Price("1").mul(2));
  }

  @Test
  public void testDiv() {
    Assert.assertEquals(Price.ZERO_PRICE, Price.ZERO_PRICE.div(1));
    assertEquals(new Price(0.5), new Price(1).div(2));
    assertEquals(new Price("-2"), new Price(-4).div(2));
    assertEquals(new Price("0.35"), new Price(0.7).div(2));
    assertEquals(new Price("-0.035"), new Price(-0.07).div(2));

    BigDecimal decimal = new BigDecimal("0.0035");
    BigDecimal two = new BigDecimal(2);
    System.out.println(decimal.divide(two, new MathContext(decimal.precision() + 3, RoundingMode.HALF_DOWN)));
  }

  @Test
  public void testIsZero() {
    assertTrue(Price.ZERO_PRICE.isZero());
    assertTrue(new Price("0").isZero());
    assertFalse(new Price("0.1").isZero());
  }

  @Test
  public void testGreaterThan() {
    assertTrue(new Price("3").greaterThan(new Price(2.999)));
    assertFalse(new Price("0").greaterThan(new Price(1)));
    assertFalse(Price.ZERO_PRICE.greaterThan(Price.ZERO_PRICE));
  }

  @Test
  public void testLessThan() {
    assertTrue(new Price("3").lessThan(new Price(3.111111111)));
    assertTrue(new Price("0").lessThan(new Price(1)));
    assertFalse(Price.ZERO_PRICE.lessThan(Price.ZERO_PRICE));
  }

  @Test
  public void testRound() {
    assertEquals(new Price(239.59).round(2), new Price(239.59));
  }
}
