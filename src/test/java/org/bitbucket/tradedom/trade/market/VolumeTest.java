package org.bitbucket.tradedom.trade.market;

import org.junit.Test;

import static org.bitbucket.tradedom.trade.market.Volume.ZERO_VOLUME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 */
public final class VolumeTest {
  @Test
  public void testEquals() {
    assertEquals(ZERO_VOLUME, ZERO_VOLUME);
    assertEquals(new Volume("12.1"), new Volume(12.1));
    assertNotEquals(new Volume("12.1"), null);
    assertNotEquals(null, new Volume("12.1"));
  }

  @Test
  public void testGreaterOrEqualsThan() {
    assertTrue(new Volume(Amount.ZERO).greaterOrEqualThan(new Volume(Amount.ZERO)));
    assertTrue(new Volume("19.34").greaterOrEqualThan(new Volume(19.34)));
    assertTrue(new Volume("19.34").greaterOrEqualThan(new Volume(18.5)));
    assertFalse(new Volume("19.34").greaterOrEqualThan(new Volume(22.5)));
  }
}
