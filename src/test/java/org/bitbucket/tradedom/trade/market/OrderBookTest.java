package org.bitbucket.tradedom.trade.market;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

/**
 *
 */
public class OrderBookTest {
  private final OrderBook btcUsdBook = new OrderBook(
    new Instrument("BTC_USD"),
    LocalDateTime.now(),
    new Asks(
      new Ask(8320.56138955, 0.00012089),
      new Ask(8321.00000254, 0.05410000),
      new Ask(8322.16555800, 0.00041699),
      new Ask(8322.79639305, 0.00050500),
      new Ask(8325.56138955, 0.00012081),
      new Ask(8326.75791890, 0.00018053),
      new Ask(8330.56138955, 0.00012074)
    ),
    new Bids(
      new Bid(8320.00000016, 0.50000000),
      new Bid(8320.00000013, 1.71700000),
      new Bid(8320.00000012, 3.40471000),
      new Bid(8320.00000000, 1.23831447),
      new Bid(8319.89919427, 0.00227054),
      new Bid(8319.88087390, 0.32000000),
      new Bid(8319.88087388, 0.31613741)
    )
  );

  @Test
  public void testBestAsk() {
    Price actual = btcUsdBook.getBestAskPrice(new Volume(0.05));
    Price expected = new Price(8321.00000254);
    assertEquals(actual, expected);
  }

  @Test
  public void testBestBid() {
    Price actual = btcUsdBook.getBestBidPrice(new Volume(2));
    Price expected = new Price(8320.00000013);
    assertEquals(actual, expected);
  }

  @Test
  public void testMaxVolumeToBuy() {
    Volume actual = btcUsdBook.getMaxVolumeToBuy(new Price(8325));
    Volume expected = new Volume(0.05514288);
    assertEquals(actual, expected);
  }

  @Test
  public void testMaxVolumeToSell() {
    Volume actual = btcUsdBook.getMaxVolumeToSell(new Price(8320));
    Volume expected = new Volume(6.86002447);
    assertEquals(actual, expected);
  }
}
